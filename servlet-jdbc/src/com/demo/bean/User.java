package com.demo.bean;

public class User {

	private String zgbh;
	private String pass;
	private String name;

	public String getZgbh() {
		return zgbh;
	}

	public void setZgbh(String zgbh) {
		this.zgbh = zgbh;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "User{" + "zgbh='" + zgbh + '\'' + ", pass='" + pass + '\'' + ", name='" + name + '\'' + '}';
	}

}
