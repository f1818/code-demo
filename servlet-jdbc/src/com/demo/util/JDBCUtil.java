package com.demo.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class JDBCUtil {

	private final static String DRIVER_CLASS_NAME = "com.mysql.jdbc.Driver";
	private final static String URL = "jdbc:mysql://localhost:3306/ospdb?useUnicode=true&characterEncoding=utf-8&useSSL=false&serverTimezone=Asia/Shanghai";
	private final static String USERNAME = "root";
	private final static String PASS_WORD = "123456";
	
	/**
	 * 获取conn
	 * @return
	 */
	public static Connection getConnection() {
		try {
			Class.forName(DRIVER_CLASS_NAME);
			return DriverManager.getConnection(URL, USERNAME, PASS_WORD);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 关闭连接
	 * （最先连接，最后关闭连接）
	 * @param rs
	 * @param ps
	 * @param conn
	 */
	public static void close(ResultSet rs, PreparedStatement ps,Connection conn){
        try{
            if(rs!=null) rs.close();
        }catch (SQLException e){
            e.printStackTrace();
        }

        try{
            if(ps!=null) ps.close();
        }catch (SQLException e){
            e.printStackTrace();
        }

        try{
            if(conn!=null) conn.close();
        }catch (SQLException e){
            e.printStackTrace();
        }

    }


}
