package com.demo.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.demo.bean.User;
import com.demo.util.JDBCUtil;

/**
 * Servlet implementation class Hello
 */
@WebServlet(description = "测试类", urlPatterns = { "/Hello" })
public class Hello extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Hello() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		  request.setCharacterEncoding("utf-8"); 
		  response.setContentType("text/html;charset=utf-8"); 
		  response.setCharacterEncoding("utf-8"); 
		  
		  Connection conn = null;
		  PreparedStatement ps =null;
		  ResultSet rs =  null;
		 
		  
		  try {
			    conn = JDBCUtil.getConnection();
		        String sql="select F_ZGBH,F_PASS,F_NAME from bsuser LIMIT 1";
		        ps = conn.prepareStatement(sql);
		        rs = ps.executeQuery();
		        
		        User user = new User();
		        
		        while(rs.next()) {
		        	user.setZgbh(rs.getString("F_ZGBH"));
		        	user.setName(rs.getString("F_NAME"));
		        	user.setPass(rs.getString("F_PASS"));
		        }
		        
		        response.getWriter().append(user.toString());
		        
		    } catch (SQLException e) {
		        e.printStackTrace();
		    }finally{
		        JDBCUtil.close(rs,ps,conn);
		    }
		  
		
	}
	
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
