package com.demo.controller;

import com.demo.pojo.Content;
import com.demo.service.ConterntService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class ContentController {

    @Autowired
    private ConterntService conterntService;

    @GetMapping("/createIndex/{index}")
    public void createIndex(@PathVariable("index") String index) throws Exception {
        conterntService.createIndex(index);
    }

    @GetMapping("/parse/{keyword}")
    public boolean parse(@PathVariable("keyword") String keyword) throws Exception {
        return conterntService.parseContent(keyword);
    }

    @GetMapping("/search/{keyword}/{pageNo}/{pageSize}")
    public List<Map<String, Object>> search(@PathVariable("keyword") String keyword,
                                            @PathVariable("pageNo") int pageNo,
                                            @PathVariable("pageSize") int pageSize) throws Exception {
        return conterntService.serarchPageHighlight(keyword, pageNo, pageSize);
    }
}
