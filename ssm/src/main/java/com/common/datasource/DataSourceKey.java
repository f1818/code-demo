package com.common.datasource;

public enum DataSourceKey {
	
	DB1("db1"),
	DB2("db2");
	
	private String name;
 
	public String getName() {
		return name;
	}
 
	public void setName(String name) {
		this.name = name;
	}
 
	private DataSourceKey(String name) {
		this.name = name;
	}


}
