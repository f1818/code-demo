package com.demo.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.demo.bean.User;
import com.demo.service.HelloService;
@Controller
@RequestMapping(value="/hello")
public class HelloController {
	
	@Autowired
	private HelloService  helloService;
	
	@ResponseBody
	@RequestMapping(value="/hello")
	public String index(HttpServletRequest req){
		
		User user = helloService.getUser();
		System.out.println("db1数据========"+user.toString());
		
		User user2 = helloService.getUser2();
		System.out.println("db2数据========"+user2.toString());
		
		User userTest = helloService.getUserTest();
		System.out.println("userTest数据（不加datasource注解）========"+userTest.toString());
		
		User userTest2 = helloService.getUserTest2();
		System.out.println("userTest数据（加datasource注解使用默认数据库）========"+userTest2.toString());
		return user.toString();
	}
	
	@ResponseBody
	@RequestMapping(value="/notTransinsert")
	public String notTransinsert(HttpServletRequest req){
		User user = new User();
		user.setName("123");
		user.setPass("123");
		user.setZgbh("123");
		try {
			helloService.notTransinsert(user);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return user.toString();
	}
	
	@ResponseBody
	@RequestMapping(value="/insertTransactional")
	public String insertTransactional(HttpServletRequest req){
		User user = new User();
		user.setName("123");
		user.setPass("123");
		user.setZgbh("123");
		
		try {
			helloService.insertTransactional(user);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return user.toString();
	}

}
